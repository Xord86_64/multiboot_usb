# What
GRUB configs based on [GLIM](https://github.com/thias/glim "GLIM"). You can change other configs in the same way to get other distros working. I just tested it on what I need for my purposes.
# Why
Use it if you want to have at least 2 partitions on flashdrive - ESP and ext4 for ISOs to pass the 4Gb limit of fat32 and get reliable boot with any linux iso.
# How
```
 ---------
|USB DRIVE|
 ---------
  /     \
 ---   ----
|ESP| |EXT4|
 ---   ----
```

1. Format flashdrive to GPT
2. Create fat32 and ext4 partitions. Set fat32 as esp (for parted `set *partition_number* esp on`)
3. Install GRUB2 on esp
4. Copy all from grub folder in /partition/mountpoint/boot/grub2 (or grub, depends on distro you installed grub from)
5. Copy ISOs into the second ext4 patrition. Don't create directoriees inside or you have to change configs.
6. Change `set iso_fs_uuid=` line in grub.cfg - set your ext4 patrition UUID
7. Try to boot it. Good luck.